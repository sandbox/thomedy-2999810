// the interface is supposed to work cross project

function _interface() {
    
    //private

    var p = new _parent();
    
    var local = this;
    
    var config = {}
    
    var dom = {};
    
    var events;
    
    
    // TODO: NEED TO MAKE SURE THERE IS A FALLBACK FOR NON CURRENT BROWSERS
    //       FOR THE AUDIO TAG
    
    // set_dom
    // params : audio element from the visualization app                   
    // functions : takes the parameter and sets the dom  in a private function
    //              that can be called from init;
    // returns : nothing
    var set_dom = function(aud_element) {
        try {
            if(!document.getElementById("testing_audio_vis_file")) {
                dom['audio_element'] = aud_element;
                document.body.appendChild(dom.audio_element);
                dom['play_button'] = document.createElement('div');
                dom.play_button.id = "play_visualization";
                document.body.appendChild(dom.play_button);
            
            // start the audio element out at 0 volume
            
                dom.audio_element.autoplay = true;
                dom.audio_element.muted = true;
                dom.audio_element.controls = true;
                dom.audio_element.loop = true;
            }
            
        }
        catch(Exception) {
            console.log("There was an error building the dom. See the interface app.");
        }

    }   
    
    // include_events
    // param : event object
    // functions allows the event creation to be built once and sent in
    //           for use as with crossbrowser reliability as import is not 
    //          standard yet ... this sets the param to a local object
    // returns : none
    var include_events = function(ev) {
        
        events = ev;
    }
    
    // TODO: BUILD A WAITING FUNCTION TO START PLAYBACK AFTER LOADED
    //        REMOVE THE SETTIMEOUT
    
    // force_playback
    // params : none
    // functions: creates a crossbrowser listener to play the music
    // returns : nothing
    var force_playback  = function() {
        
        try {
            if(!document.getElementById("testing_audio_vis_file")) {
             window.setTimeout(function() {
                  dom.play_button.click(); 
//                  console.log('muted functionality in force_playback function from audio_visualizer.js is commented out i');
                  dom.audio_element.muted = false;
             }, 5000);
            }
        }
        catch(Exception) {
            console.log('The forced playback in the interface app is having difficulty!');
        }
    }
    
    
    // public
    
    // init
    // param: none
    // functions calls all the necessary functionality to create an easier api
    // returns : local for chaining
    local.init = function(audio_element, events) {
        
        
        set_dom(audio_element);
        
        include_events(events);
  
        force_playback();
        
        return local;
    }
    
    // get_settings
    // params : setting_name  defaults to all
    // functions returns the parents configuration settings for use later if the param utilizes default it returns the whole config settings otherwise returns the named setting
    // returns parent config
    local.get_setting = function (setting_name = "all") {
         return p.get_setting(setting_name);
    }

    // get_dom
    // params : setting_name  defaults to all
    // functions returns the parents configuration settings for use later if the param utilizes default it returns the whole config settings otherwise returns the named setting
    //  returns dom config
    local.get_dom = function (setting_name = "all") {
         return (setting_name == 'all') ? dom : dom[setting_name];
    }
    
    // get_song_title 
    // param: none
    // functions : it grabs the song title from the dom and then returns just the title
    // returns : song title
    local.get_song_title = function() {
        return document.getElementById('audio_vis_track_title').innerText;
    }
    // Debug
    // parameter : all is a boolean for all settings or 1 specific setting
    // parameter : setting name is a string that takes a specific setting name 
    //             from the parent object and logs it to the console
    //             it is coded/designed to get all settings and should include 
    //             an intentional name for specific settings
    // returns the local object for chaining
    local.debug = function (all = true, setting_name = null ) {
        if(all) {
            console.log("Config settings", p.get_setting());
        }
        else {
            console.log("Config settings", p.get_setting(setting_name));
        }
        
        return local;
    }
    
    // TODO : USE AS A CALLBACK UNTIL WE FIND A BETTER SOLUTION
    //        GOAL - TAKE ADVANTAGE OF THE EXISTING RAF LOOP
    
    // visualize 
    // params : data contains the audio data to draw
    // functions : creates a callback to visualize the audio and called in 
    //              visulization.js
    // returns : none
    local.visualize = function(audio_data) {
        console.log(audio_data);
    }
    
        
}
