"use strict"

function main() {
    var visualizer = new _visualizer();
    
    var intfce = new _interface();
    
    
    visualizer.init(intfce.get_song_title());
    
    
    intfce.init(visualizer.get_setting('audio_element'), events);
    
    visualizer.make_friends(intfce);
    
    events.add(intfce.get_dom('play_button'), 'click', function() {
          visualizer.get_setting('audio_element').play();
          visualizer.play();
    })
    

}

events.add(window, "load", main());